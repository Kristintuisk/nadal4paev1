﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// using System.Data.Sql; ei pea lisama, sest on tehtud entity class

namespace SoojendusharjutuseAken
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // siia kategooriate nimed
        // täidetakse kohe akna avamisel
        // määra datasource

        private void Form1_Load(object sender, EventArgs e) // mida teha akna avamisel ( e load)
        {
            NorthwindEntities db = new NorthwindEntities();  // pane data sourceks loetelu, mida näidata
            this.KategooriateNimed.DataSource = db.Categories
                .Select(x => x.CategoryName)
                .ToList()
                ;
        }
        private void KategooriateNimed_SelectedIndexChanged(object sender, EventArgs e)  
            // object senderis on tuletatud kõik muud klassid (listboxid jms)
            // et saada sealt kätte, on vaja castingutehet
        {
            string categoryName = ((ListBox)sender).SelectedItem.ToString();  
            NorthwindEntities db = new NorthwindEntities();

            #region Kommentaarid
            private void KategooriateNimed_SelectedIndexChanged(object sender, EventArgs e)  // Hennul listbox
                                                                                             // object senderis on tuletatud kõik muud klassid (listboxid jms)
                                                                                             // et saada sealt kätte, on vaja castingutehet
            {
                string categoryName = ((ListBox)sender).SelectedItem.ToString();  // vaata senderit kui listboxi ja lase küsida selected itemit
                                                                                  // products on category propertid
                NorthwindEntities db = new NorthwindEntities(); 
                #endregion

                this.dataGridView1.DataSource = 
            db.Categories.Where(x => x.CategoryName == categoryName)  
                .Single().Products.Select(x => new { x.ProductID, x.ProductName, x.UnitPrice }) 
                .ToList();  
           dataGridView1.Columns[2].DefaultCellStyle.Format = "F2"; 
                                                                    

            #region Kommentaarid
            this.dataGridView1.DataSource = // 2. omistan 1. lause tulemuse muutujale
                db.Categories.Where(x => x.CategoryName == categoryName)  // muutuja db, where operatsiooni tulemuseks on mingi kogus
                    .Single().Products.Select(x => new { x.ProductID, x.ProductName, x.UnitPrice }) // 1. võtame selle ühe kategooria ja selle seest id, nime ja hinna
                    .ToList();  // eelmine rida - tahan, et sisse tulevad tooted, aga välja lähevad asjad, mis koosnevad nimest, IDst ja hinnast
            dataGridView1.Columns[2].DefaultCellStyle.Format = "F2"; // teise tulba (0st alates) formaat on kahe komakohaga (datagrid view display format)
                                                                     // siis saab UnitPricega pärast veel edasi toimetada 
            #endregion

            dataGridView1.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                #region Kommentaarid
                // eelmise väärtus on enum nimega DataGridViewContentAlignment
                // näiteks: dataGridView1.Columns[0].HeaderText = "Tootekood";
                // new on anonüümne struct, tahab, et asjadel oelksid nimed (Product ID jms)
                // küsimärgi selgitus kuula SALVESTUSELT = kui hinda pole, siis jääb tühjaks UnitPrice = x.UnitPrice?.ToString("F2")
                // unitprice on nullable tüüpi (baasis võib olla tooteid, millele hinda pole veel pandud) - sellepärast on value tehtud stringiks kahe komakohaga
                // UnitPrice = x.UnitPrice.Value.ToString("F2")
                // single - massiivist v listist võtan esimese elemendiiindeksiga 0, torust tulevad kategooriad ja single küsib sealt selle üheainukese, mis tuleb selle nimega

                #endregion
            }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

